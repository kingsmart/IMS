/*
Navicat MySQL Data Transfer

Source Server         : mycon
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : ims

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2017-02-09 13:53:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_catalog`
-- ----------------------------
DROP TABLE IF EXISTS `sys_catalog`;
CREATE TABLE `sys_catalog` (
  `catalog_id` varchar(50) NOT NULL DEFAULT '' COMMENT '分类科目编号',
  `cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `root_key` varchar(100) DEFAULT NULL COMMENT '科目标识键',
  `root_name` varchar(100) DEFAULT NULL COMMENT '科目名称',
  `catalog_name` varchar(50) DEFAULT NULL COMMENT '分类名称',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父节点编号',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '图标名称',
  `is_auto_expand` varchar(10) DEFAULT '0' COMMENT '是否自动展开',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`catalog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类科目';

-- ----------------------------
-- Records of sys_catalog
-- ----------------------------
INSERT INTO `sys_catalog` VALUES ('0e6cca42523b4f95afb8d138dc533e61', '0.002', 'DIC_TYPE', '字典分类科目', '数据字典分类', '0', '1', 'book', '0', '2016-10-07 23:49:14', null, '2016-10-07 23:49:31', null);
INSERT INTO `sys_catalog` VALUES ('4f39839093744dccaedc9d7dcdee4ab3', '0.001', 'PARAM_TYPE', '参数分类', '参数分类科目', '0', '1', 'book', '0', '2016-10-07 23:46:41', null, '2016-10-07 23:47:57', null);
INSERT INTO `sys_catalog` VALUES ('5423b9ba9ac6472c80881827acafe9e9', '0.001.001', 'PARAM_TYPE', '参数分类', '系统参数', '4f39839093744dccaedc9d7dcdee4ab3', '1', 'folder', '1', '2016-10-07 23:47:50', null, null, null);
INSERT INTO `sys_catalog` VALUES ('6a75051434b840a597aeb549e1e47ced', '0.002.001', 'DIC_TYPE', '字典分类科目', '系统管理', '0e6cca42523b4f95afb8d138dc533e61', '2', 'folder', '1', '2016-10-07 23:50:13', null, '2016-10-07 23:57:03', null);
INSERT INTO `sys_catalog` VALUES ('b420860910f54c1d8901f099a9457f52', '0.002.002', 'DIC_TYPE', '字典分类科目', '全局通用', '0e6cca42523b4f95afb8d138dc533e61', '1', 'global', '1', '2016-10-08 00:01:11', null, null, null);

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` varchar(50) NOT NULL COMMENT '流水号',
  `cascade_id` varchar(255) NOT NULL COMMENT '节点语义ID',
  `dept_name` varchar(100) NOT NULL COMMENT '组织名称',
  `parent_id` varchar(50) NOT NULL COMMENT '父节点流水号',
  `dept_code` varchar(50) DEFAULT NULL COMMENT '机构代码',
  `manager` varchar(50) DEFAULT NULL COMMENT '主要负责人',
  `phone` varchar(50) DEFAULT NULL COMMENT '部门电话',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `is_auto_expand` varchar(10) DEFAULT NULL COMMENT '是否自动展开',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '节点图标文件名称',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `is_del` varchar(10) DEFAULT '0' COMMENT '是否已删除 0有效 1删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('0', '0', '组织机构', '-1', null, null, null, null, null, '1', 'dept_config', '1', '顶级机构不能进行移动和删除操作，只能进行修改', '0', '2016-12-29 10:13:20', null, null, null);
INSERT INTO `sys_dept` VALUES ('02448fead1be410ab4f0f7bea7285448', '0.0001', '广州研发中心', '0', null, null, null, null, null, '1', null, '1', null, '0', '2017-01-16 14:03:01', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:03:01', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dept` VALUES ('728f6b0a20f0492dbd97b871620c2b19', '0.0001.0001', '天河区办事处', '02448fead1be410ab4f0f7bea7285448', null, null, null, null, null, '1', null, '1', null, '0', '2017-01-16 14:03:16', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:03:16', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dept` VALUES ('fdc7155b279b47f58439c514fade3d8a', '0.0001.0002', '白云区办事处', '02448fead1be410ab4f0f7bea7285448', '', '', '', '', '', '1', '', '2', '', '1', '2017-01-16 14:03:32', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:53:27', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `sys_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `dic_id` varchar(50) NOT NULL COMMENT '字典编号',
  `dic_index_id` varchar(255) DEFAULT NULL COMMENT '所属字典流水号',
  `dic_code` varchar(100) DEFAULT NULL COMMENT '字典对照码',
  `dic_value` varchar(100) DEFAULT NULL COMMENT '字典对照值',
  `show_color` varchar(50) DEFAULT NULL COMMENT '显示颜色',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典';

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------
INSERT INTO `sys_dictionary` VALUES ('11b823f3b2e14e76bf94347a4a5e578e', 'c48507ef391d4e3d8d9b7720efe4841b', '0', '停用', null, '1', '0', '1', '2016-10-07 23:54:37', null, '2016-10-11 02:20:16', null);
INSERT INTO `sys_dictionary` VALUES ('293adbde400f457a8d947ff5c6341b04', '992a7d6dbe7f4009b30cbae97c3b64a9', '3', '锁定', '#FFA500', '1', '1', '3', '2016-12-18 21:33:42', null, '2017-01-15 21:47:24', null);
INSERT INTO `sys_dictionary` VALUES ('2ac97527c4924127b742dd953d8b53ba', '820d2a68425b4d8d9b423b81d6a0eec1', '3', '未知', null, '1', '1', '3', '2016-10-08 22:45:47', null, '2016-12-18 22:44:29', null);
INSERT INTO `sys_dictionary` VALUES ('2bfc90a6917545cd87d73fb491292e2b', 'aaec0092a25b485f90c20898e9d6765d', '1', '缺省', '', '1', '1', '1', '2016-12-31 09:19:48', null, '2017-01-16 17:54:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('3cf6af08f48e4cec913d09f67a0b3b43', '992a7d6dbe7f4009b30cbae97c3b64a9', '1', '正常', '', '1', '1', '1', '2016-12-18 09:03:21', null, '2016-12-27 17:48:36', null);
INSERT INTO `sys_dictionary` VALUES ('82afb0bda8944af3a0e5f82608294670', '0bf2a3cd7ed44516a261347d47995411', '2', '顶部布局', null, '1', '1', '2', '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317', '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('913ca1b4b49a434fb9591f6df0a52af8', 'c6f8b99b95c844b89dc86c143e04a294', '0', '否', null, '1', '0', '1', '2016-10-07 23:53:45', null, '2016-10-11 02:20:43', null);
INSERT INTO `sys_dictionary` VALUES ('9c63657b98c444e3bfd8a0a75128de2b', '7a7faf68a5ec4f3cb9f45d89c119b26b', '0', '只读', null, '1', '0', '1', '2016-10-07 23:55:13', null, '2016-10-11 02:20:06', null);
INSERT INTO `sys_dictionary` VALUES ('a96dfb72b7b54e1989569a2b3c5f90ac', 'c48507ef391d4e3d8d9b7720efe4841b', '1', '启用', null, '1', '0', '1', '2016-10-07 23:54:47', null, '2016-10-11 02:20:22', null);
INSERT INTO `sys_dictionary` VALUES ('ca40ef37acef49f8930fcf22356ba50e', 'c6f8b99b95c844b89dc86c143e04a294', '1', '是', null, '1', '0', '2', '2016-10-07 23:54:09', null, '2016-10-11 02:20:32', null);
INSERT INTO `sys_dictionary` VALUES ('d2cf230ce49040e3bf6e61a972659c09', '992a7d6dbe7f4009b30cbae97c3b64a9', '2', '停用', 'red', '1', '1', '2', '2016-12-18 21:33:25', null, '2016-12-19 21:09:04', null);
INSERT INTO `sys_dictionary` VALUES ('d404e540aab945df84a26e3d30b2dd47', '820d2a68425b4d8d9b423b81d6a0eec1', '2', '女', null, '1', '1', '2', '2016-10-08 22:47:09', null, '2016-12-18 22:44:17', null);
INSERT INTO `sys_dictionary` VALUES ('d7f0c4a5480d4dc4b3e6e4c5b405d9cb', '820d2a68425b4d8d9b423b81d6a0eec1', '1', '男', null, '1', '1', '1', '2016-10-08 22:46:49', null, '2016-12-18 22:44:42', null);
INSERT INTO `sys_dictionary` VALUES ('e0e59a52f42c4263aac3e9dbbdb496df', '0bf2a3cd7ed44516a261347d47995411', '1', '经典风格', null, '1', '1', '1', '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317', '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('f1c0ae8844504f96836b904ce81ac1bc', '7a7faf68a5ec4f3cb9f45d89c119b26b', '1', '可编辑', null, '1', '0', '2', '2016-10-07 23:55:25', null, '2016-10-11 02:19:52', null);

-- ----------------------------
-- Table structure for `sys_dictionary_index`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary_index`;
CREATE TABLE `sys_dictionary_index` (
  `dic_index_id` varchar(50) NOT NULL COMMENT '流水号',
  `dic_key` varchar(50) DEFAULT NULL COMMENT '字典标识',
  `dic_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `catalog_id` varchar(50) DEFAULT NULL COMMENT '所属分类流水号',
  `catalog_cascade_id` varchar(500) DEFAULT NULL COMMENT '所属分类流节点语义ID',
  `dic_remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dic_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典索引表';

-- ----------------------------
-- Records of sys_dictionary_index
-- ----------------------------
INSERT INTO `sys_dictionary_index` VALUES ('0bf2a3cd7ed44516a261347d47995411', 'layout_style', '界面布局', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317', '2016-02-17 14:29:30', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('7a7faf68a5ec4f3cb9f45d89c119b26b', 'edit_mode', '编辑模式', 'b420860910f54c1d8901f099a9457f52', '0.002.002', '', '2016-10-07 10:49:16', null, '2016-10-08 00:01:55', null);
INSERT INTO `sys_dictionary_index` VALUES ('820d2a68425b4d8d9b423b81d6a0eec1', 'sex', '性别', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2016-10-08 22:45:28', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('992a7d6dbe7f4009b30cbae97c3b64a9', 'user_status', '用户状态', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2016-12-18 08:57:02', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('aaec0092a25b485f90c20898e9d6765d', 'role_type', '角色类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2016-12-31 09:19:18', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('c48507ef391d4e3d8d9b7720efe4841b', 'status', '当前状态', 'b420860910f54c1d8901f099a9457f52', '0.002.002', '', '2016-10-07 11:08:12', null, '2016-10-08 00:01:50', null);
INSERT INTO `sys_dictionary_index` VALUES ('c6f8b99b95c844b89dc86c143e04a294', 'is_auto_expand', '是否自动展开', 'b420860910f54c1d8901f099a9457f52', '0.002.002', '', '2016-10-07 13:09:25', null, '2016-10-08 00:01:44', null);

-- ----------------------------
-- Table structure for `sys_icon`
-- ----------------------------
DROP TABLE IF EXISTS `sys_icon`;
CREATE TABLE `sys_icon` (
  `icon_id` varchar(50) NOT NULL COMMENT '图标编号',
  `icon_name` varchar(50) NOT NULL COMMENT '图标名称',
  `icon_file_name` varchar(50) DEFAULT NULL,
  `icon_path` varchar(100) DEFAULT NULL,
  `icon_type` varchar(50) DEFAULT NULL,
  `icon_size_type` varchar(10) DEFAULT NULL COMMENT '图标尺寸类型 1:16*16 2:24*24 3:32*32  4:其它',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态 0 停用 1启用',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '图标创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '图标修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`icon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图标信息';

-- ----------------------------
-- Records of sys_icon
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单编号',
  `cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `menu_name` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '菜单父级编号',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '图标名称',
  `is_auto_expand` varchar(10) DEFAULT '0' COMMENT '是否自动展开',
  `url` varchar(100) DEFAULT NULL COMMENT 'url地址',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单配置';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1ae9eeb251a243abb4c0c4f3865d6262', '0.001.001', '菜单配置', 'c66886c9ee47415aa81a6589acdb480a', 'menu_config', '1', 'system/menu/init.jhtml', null, '1', '1', '4', '2016-10-07 21:23:38', null, null, null);
INSERT INTO `sys_menu` VALUES ('8eefe6e3298c4203b21e85e354b284ab', '0.001.004', '分类科目', 'c66886c9ee47415aa81a6589acdb480a', 'catalog', '1', 'system/catalog/init.jhtml', null, '1', '1', '7', '2016-10-09 21:23:47', null, null, null);
INSERT INTO `sys_menu` VALUES ('94ca69dd59b84054ad056b130d959425', '0.001.003', '键值参数', 'c66886c9ee47415aa81a6589acdb480a', 'param_config', '1', 'system/param/init.jhtml', null, '1', '1', '6', '2016-10-09 21:22:03', null, null, null);
INSERT INTO `sys_menu` VALUES ('a5b39c90931b4249a23e30f24303dbfa', '0.001.002', '数据字典', 'c66886c9ee47415aa81a6589acdb480a', 'dictionary', '1', 'system/dictionary/init.jhtml', '', '1', '0', '5', '2016-10-07 21:40:52', null, '2016-10-07 22:12:51', null);
INSERT INTO `sys_menu` VALUES ('c66886c9ee47415aa81a6589acdb480a', '0.001', '系统管理', '0', 'system_manage', '1', '', '', '1', '1', '10', '2016-10-07 17:57:14', null, '2017-02-09 11:54:32', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('d525b1f3274244c9af3a06c4e72621d8', '0.001.009', '角色管理', 'c66886c9ee47415aa81a6589acdb480a', 'group_link', '1', 'system/role/init.jhtml', '', '1', '1', '3', '2016-12-31 09:09:32', null, '2017-01-15 19:32:18', null);
INSERT INTO `sys_menu` VALUES ('dfc4df3c7f4341f885caf7aa305f8995', '0.001.007', ' 组织机构', 'c66886c9ee47415aa81a6589acdb480a', 'dept_config', '1', 'system/dept/init.jhtml', '', '1', '1', '1', '2016-12-11 08:38:33', null, '2016-12-11 21:59:55', null);
INSERT INTO `sys_menu` VALUES ('f278c724ce8e4649bc2b74333ac0d28c', '0.001.008', '用户管理', 'c66886c9ee47415aa81a6589acdb480a', 'user_config', '1', 'system/user/init.jhtml', '', '1', '1', '2', '2016-12-18 08:41:02', null, '2017-01-15 20:03:32', null);

-- ----------------------------
-- Table structure for `sys_param`
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `param_id` varchar(50) NOT NULL DEFAULT '' COMMENT '参数编号',
  `param_name` varchar(100) DEFAULT NULL COMMENT '参数名称',
  `param_key` varchar(50) DEFAULT NULL COMMENT '参数键名',
  `param_value` varchar(500) DEFAULT NULL COMMENT '参数键值',
  `catalog_id` varchar(50) DEFAULT NULL COMMENT '目录ID',
  `catalog_cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `param_remark` varchar(200) DEFAULT NULL COMMENT '参数备注',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='键值参数';

-- ----------------------------
-- Records of sys_param
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` varchar(50) NOT NULL COMMENT ' 流水号',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态 1启用 0禁用',
  `role_type` varchar(10) DEFAULT NULL COMMENT '角色类型',
  `role_remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(50) NOT NULL COMMENT ' 角色流水号',
  `menu_id` varchar(50) NOT NULL COMMENT '功能模块流水号',
  `grant_type` varchar(10) DEFAULT NULL COMMENT '权限类型 1 经办权限 2管理权限',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单模块-角色关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `role_id` varchar(50) NOT NULL COMMENT '角色编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与用户关联';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `account` varchar(50) NOT NULL COMMENT '用户登录帐号',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `username` varchar(50) NOT NULL COMMENT '用户姓名',
  `lock_num` int(10) DEFAULT '5' COMMENT '锁定次数 默认5次',
  `error_num` int(10) DEFAULT '0' COMMENT '密码错误次数  如果等于锁定次数就自动锁定用户',
  `sex` varchar(10) DEFAULT '3' COMMENT '性别  1:男2:女3:未知',
  `status` varchar(10) DEFAULT '1' COMMENT '用户状态 1:正常2:停用 3:锁定',
  `user_type` varchar(10) DEFAULT NULL COMMENT '用户类型',
  `dept_id` varchar(50) DEFAULT NULL COMMENT '所属部门流水号',
  `mobile` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'QQ号码',
  `wechat` varchar(50) DEFAULT NULL COMMENT '微信',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮件',
  `idno` varchar(50) DEFAULT NULL COMMENT '身份证号',
  `style` varchar(10) DEFAULT '1' COMMENT '界面风格',
  `address` varchar(200) DEFAULT NULL COMMENT '联系地址',
  `remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `is_del` varchar(10) DEFAULT '0' COMMENT '是否已删除 0有效 1删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户编号',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('09da195ca64a40769942c1ef27c302ab', 'root', 'qPHI0Wji8RE=', '测试', '5', '0', '3', '1', '1', '0', '', '', '', '', '', '1', '', '', '0', '2017-01-15 20:04:00', null, '2017-01-16 23:23:05', '09da195ca64a40769942c1ef27c302ab');
INSERT INTO `sys_user` VALUES ('0fe03d0d834b41be88dcd132b99b4ba7', 'system', 'FyzQw6C0Ct4=', '用户', '5', '0', '3', '1', '1', '02448fead1be410ab4f0f7bea7285448', '', '', '', '', '', '1', '', '', '1', '2017-01-15 21:32:12', null, '2017-02-09 11:52:41', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_user` VALUES ('15d8636a48904d469954152b5cf80d13', 'test1', 'FyzQw6C0Ct4=', '1', '5', '0', '3', '1', '1', '0', null, null, null, null, null, '1', null, null, '1', '2017-01-16 14:56:45', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:57:16', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_user` VALUES ('55cf332c2e4a427591146dbacc3c3043', 'test', 'FyzQw6C0Ct4=', '测试', '5', '0', '1', '1', '1', 'fdc7155b279b47f58439c514fade3d8a', null, null, null, null, null, '1', null, null, '1', '2017-01-16 14:25:34', null, '2017-01-16 14:53:27', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_user` VALUES ('77ddeee5df2c462cbbb3995204c37d01', 'test', 'FyzQw6C0Ct4=', '测试', '5', '0', '3', '1', '1', '728f6b0a20f0492dbd97b871620c2b19', null, null, null, null, null, '1', null, null, '1', '2017-01-16 14:53:45', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:57:31', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_user` VALUES ('cb33c25f5c664058a111a9b876152317', 'super', '0d+ywCe6ffI=', '超级用户', '10', '0', '1', '1', '2', '0', '13802907704', '240823329', '', '240823329@qq.com', '', '2', '', '超级用户，拥有最高的权限', '0', '2017-01-15 09:47:46', null, '2017-02-09 11:56:20', 'cb33c25f5c664058a111a9b876152317');
